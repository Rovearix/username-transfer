package me.rovearix.usernametransfer;

import me.rovearix.usernametransfer.utils.LookupEvent;
import me.rovearix.usernametransfer.utils.TransferUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Listeners implements Listener {
    public static UsernameTransfer plugin;

    public Listeners(UsernameTransfer plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {

        //Checks for cache for previous logins
        if (!TransferUtils.checkNameCache(event.getPlayer().getName())) {

            //Separate thread for Transferring because of external web server interactions
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new LookupEvent(event.getPlayer(), plugin));

            //Logs new player
            TransferUtils.writeNameCache(event.getPlayer().getName());
        }
    }
}