package me.rovearix.usernametransfer.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;

public class NameLookup {

    //Mojang API URL that provides previous usernames
    private static final String LOOKUP_URL = "https://api.mojang.com/user/profiles/%s/names";

    //Mojang API URL that provides player uuid
    private static final String GET_UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s?t=0";

    //JSON Parser
    private static final JsonParser JSON_PARSER = new JsonParser();

    //Returns an array with all players past usernames
    public static String[] getPlayerPreviousNames(String uuid) throws IOException {
        if (uuid == null || uuid.isEmpty())
            return null;
        String response = getRawJsonResponse(new URL(String.format(LOOKUP_URL, uuid)));
        JsonArray jsonArray = JSON_PARSER.parse(response).getAsJsonArray();
        String[] names = new String[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject object = jsonArray.get(i).getAsJsonObject();
            names[i] = object.get("name").getAsString();
        }
        return names;
    }

    //Gets the UUID of a player
    public static String getPlayerUUID(String name) throws IOException {
        String response = getRawJsonResponse(new URL(String.format(GET_UUID_URL, name)));
        JsonObject object = JSON_PARSER.parse(response).getAsJsonObject();
        if (object == null)
            return null;
        return object.get("id") == null ? null : object.get("id").getAsString();
    }

    //Gets a response from Mojang's API webservers.
    private static String getRawJsonResponse(URL u) throws IOException {
        HttpURLConnection con = (HttpURLConnection) u.openConnection();
        con.setDoInput(true);
        con.setConnectTimeout(2000);
        con.setReadTimeout(2000);
        con.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String response = in.readLine();
        in.close();
        return response;
    }
}