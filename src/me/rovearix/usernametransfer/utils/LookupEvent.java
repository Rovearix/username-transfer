package me.rovearix.usernametransfer.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.IOException;

public class LookupEvent implements Runnable {

    private Player player;
    private Plugin plugin;
    private String[] previousNames;

    public LookupEvent(Player player, Plugin plugin) {
        this.player = player;
        this.plugin = plugin;
    }

    //Async method to check Mojang for player name history
    public void run() {
        try {

            //Array to hold the previous player names
            previousNames = NameLookup.getPlayerPreviousNames(NameLookup.getPlayerUUID(player.getName()));

            //Checks if the player has played before
            if (previousNames != null && previousNames.length != 1 && TransferUtils.checkPlayerData(previousNames[previousNames.length - 2])) {

                //Brings the kick and transfer back to sync with the server
                Bukkit.getScheduler().runTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            player.kickPlayer("Transferring your old user data, please relog.");
                            TransferUtils.transfer(player.getName(), previousNames[previousNames.length - 2], player);
                            Thread.sleep(5000);
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex reload");
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "econ reload");
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "essentials reload");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}