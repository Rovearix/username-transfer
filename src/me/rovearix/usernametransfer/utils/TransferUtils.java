package me.rovearix.usernametransfer.utils;

import me.rovearix.usernametransfer.UsernameTransfer;
import org.bukkit.command.CommandSender;

import java.io.*;

public class TransferUtils {

    //Sends the usernames to the batch file for transferring
    public static void transfer(String newUsername, String oldUsername, CommandSender sender) {
        try {
            Runtime.getRuntime().exec("powershell.exe " + UsernameTransfer.LOCATION + "transfer.ps1 " + newUsername + " " + oldUsername);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Checks if there is player data for the player
    public static boolean checkPlayerData(String username) {
        File folder = new File("Wild/players");
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null)
            for (File file : listOfFiles)
                if (file.isFile() && file.getName().equals(username + ".dat"))
                    return true;
        return false;
    }

    //Checks player name cache if played before
    public static boolean checkNameCache(String username) {
        try {

            //Buffer for reading cahce
            BufferedReader reader = new BufferedReader(new FileReader(UsernameTransfer.LOCATION + "namecache.txt"));

            //Checks if the player is on the list
            String name = reader.readLine();
            while (name != null) {
                if (name.equals(username))
                    return true;
                name = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Returns false if player not found
        return false;
    }

    //Logs username in name cache
    public static void writeNameCache(String username) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(UsernameTransfer.LOCATION + "namecache.txt", true));
            out.write(username + "\n");
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}