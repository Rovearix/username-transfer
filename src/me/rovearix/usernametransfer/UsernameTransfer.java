package me.rovearix.usernametransfer;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class UsernameTransfer extends JavaPlugin {

    public static String LOCATION;

    //Enabling the plugin
    @Override
    public void onEnable() {
        getLogger().info("Rovearix was here <3");
        LOCATION = getDataFolder().getAbsolutePath() + File.separator;
        this.saveResource("transfer.ps1", true);
        this.saveResource("namecache.txt", false);
        Bukkit.getPluginManager().registerEvents(new Listeners(this), this);
    }
}