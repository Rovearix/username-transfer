#!/bin/bash

# Store the usernames
newUser=$1
oldUser=$2

# Transfers the factions data
mv -f mstore/factions_uplayer\@default/${newUser,,}.json mstore/factions_uplayer\@default/${newUser,,}.json.backup
mv mstore/factions_uplayer\@default/${oldUser,,}.json mstore/factions_uplayer\@default/${newUser,,}.json

# Transfers the BOSEconomy data
sed -i -e "/$newUser {/,+3d" plugins/BOSEconomy/accounts.txt
sed -i -e "s/$oldUser/$newUser/g" plugins/BOSEconomy/accounts.txt

# Transfers the PermissionsEX data
sed -i -e "s/$oldUser:/$newUser:/g" plugins/PermissionsEx/permissions.yml

# Transfers the world player data
mv -f Wild/players/$newUser.dat Wild/players/$newUser.dat.backup
mv Wild/players/$oldUser.dat Wild/players/$newUser.dat

# Transfers the Essentials data
mv -f plugins/Essentials/userdata/${newUser,,}.yml plugins/Essentials/userdata/${newUser,,}.yml.backup
mv plugins/Essentials/userdata/${oldUser,,}.yml plugins/Essentials/userdata/${newUser,,}.yml