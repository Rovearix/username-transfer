# Store the usernames
$newUser=$args[0]
$oldUser=$args[1]

# Transfers the world player data
Move-Item -Force Wild\players\$newUser.dat Wild\players\$newUser.dat.backup
Move-Item Wild\players\$oldUser.dat Wild\players\$newUser.dat

# Transfers the BOSEconomy data
(Get-Content plugins\BOSEconomy\accounts.txt) | foreach {$_.replace($newUser,$newUser + "CoolaxPreviousBankAccount")} | Set-Content plugins\BOSEconomy\accounts.txt
(Get-Content plugins\BOSEconomy\accounts.txt) | foreach {$_.replace($oldUser,$newUser)} | Set-Content plugins\BOSEconomy\accounts.txt

# Transfers the PermissionsEX data
(Get-Content plugins\PermissionsEx\permissions.yml) | foreach {$_.replace($oldUser,$newUser)} | Set-Content plugins\PermissionsEx\permissions.yml

# Converts variables to lowercase becuase of plugin file storage
$newUser=$newUser.ToLower()
$oldUser=$oldUser.ToLower()

# Transfers the Factions data
Move-Item -Force mstore\factions_uplayer@default\$newUser.json mstore\factions_uplayer@default\$newUser.json.backup
Move-Item mstore\factions_uplayer@default\$oldUser.json mstore\factions_uplayer@default\$newUser.json

# Transfers the Essentials data
Move-Item -Force plugins\Essentials\userdata\$newUser.yml plugins\Essentials\userdata\$newUser.yml.backup
Move-Item plugins\Essentials\userdata\$oldUser.yml plugins\Essentials\userdata\$newUser.yml